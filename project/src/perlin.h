#ifndef PERLIN_H
#define PERLIN_H

#include "data_types.h"

extern f64 rawnoise(i32 n);

extern f64 noise1d(i32 x, i32 octave, i32 seed);

extern f64 noise2d(i32 x, i32 y, i32 octave, i32 seed);

extern f64 noise3d(i32 x, i32 y, i32 z, i32 octave, i32 seed);

extern f64 interpolate(f64 a, f64 b, f64 t);

extern f64 smooth1d(f64 x, i32 octave, i32 seed);

extern f64 smooth2d(f64 x, f64 y, i32 octave, i32 seed);

extern f64 smooth3d(f64 x, f64 y, f64 z, i32 octave, i32 seed);

extern f64 pnoise1d(f64 x, f64 persistence, i32 octaves, i32 seed);

extern f64 pnoise2d(f64 x, f64 y, f64 persistence, i32 octaves, i32 seed);

extern f64 pnoise3d(f64 x, f64 y, f64 z, f64 persistence, i32 octaves, i32 seed);

extern f64 normalize1d(f64 input);

extern f64 sigmoid(f64 input);

#endif /* PERLIN_H */
