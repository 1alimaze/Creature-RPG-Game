#include <GL/gl.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include "block/block.h"
#include "cglm/vec3.h"
#include "state.h"
#include "errors.h"
#include "texture.h"
#include "stb/stb_image.h"
#include "shader_utils.h"
#include "perlin.h"
#include "uthash.h"
#include "util.h"

static GLuint program;
static Texture tile_atlas;
GLint attribute_coord;
GLint uniform_mvp;

void update_vectors(f64 dx, f64 dy)
{
	state.world.forward[0] = sinf(state.world.angle[0]);
	state.world.forward[1] = 0;
	state.world.forward[2] = cosf(state.world.angle[0]);

	state.world.right[0] = -cosf(state.world.angle[0]);
	state.world.right[1] = 0;
	state.world.right[2] = sinf(state.world.angle[0]);

	state.world.lookat[0] = sinf(state.world.angle[0]) * cosf(state.world.angle[1]);
	state.world.lookat[1] = sinf(state.world.angle[1]);
	state.world.lookat[2] = cosf(state.world.angle[0]) * cosf(state.world.angle[1]);

	glm_cross(state.world.right, state.world.lookat, state.world.up);
}

#define SEA_LEVEL 8

u16 get_ground_block(u8 y, f64 n, f64 r)
{
	/*if (y == n && y >= SEA_LEVEL)
		return GRASS;
	if (r > 0.5f)
		return (i32)(n * 2) < SEA_LEVEL ? DIRT : GRASS;
	if (n < SEA_LEVEL + 2 && r < -0.5f)
		return SAND;
	if (r < 1.25f)
		return STONE;
	return WATER;*/
	if (y == (i32)n)
		return GRASS;
	if (r < -0.5f)
		return STONE;
	if (r < 0.0f)
		return SAND;
	return DIRT;
}

Chunk* chunk_generate(World* w, ivec2 map_pos)
{
	Chunk* c = malloc(sizeof(Chunk));
	memset(c, 0, sizeof(Chunk));
	// Negatives don't work for some reason
	// Figured it out it was i32 casting F x86 clang
	for (u8 x = 0; x < CHUNK_SIZE_X; x++) {
		for (u8 z = 0; z < CHUNK_SIZE_Z; z++) {
			double n = CHUNK_SIZE_Y*sigmoid(2*pnoise2d(map_pos[0] + x/16.0f, map_pos[1] + z/16.0f, 0.8f, 5, w->seed.height_map));
			u8 y = n;
			for (u8 w = SEA_LEVEL; w > y; w--)
				chunk_get(c, x, w, z) = WATER;
			do
				chunk_get(c, x, y, z) = get_ground_block(y, n, pnoise3d(map_pos[0] + x/16.0f, (float)y/CHUNK_SIZE_Y, map_pos[1] + z/16.0f, 0.1f, 2, w->seed.height_map));
			while (y--);
		}
	}
	c->elements = 0;
    c->changed = true;
    c->need_to_save = false;
    c->is_queued = false;
    glGenBuffers(1, &c->vbo);
	c->id.x = map_pos[0];
	c->id.y = map_pos[1];
	Chunk* generated_chunk = c;
    HASH_ADD(hh, w->chunks, id, sizeof(ivec2s), c);

    // Update neighbors
    ivec2s pos = c->id;
    pos.x++;
	HASH_FIND(hh, w->chunks, pos.raw, sizeof(ivec2s), c);
	if (c)
		chunk_queue_push(&w->cq, c);
	pos.x -= 2;
	HASH_FIND(hh, w->chunks, pos.raw, sizeof(ivec2s), c);
	if (c)
		chunk_queue_push(&w->cq, c);
	pos.x++;
	pos.y++;
	HASH_FIND(hh, w->chunks, pos.raw, sizeof(ivec2s), c);
	if (c)
		chunk_queue_push(&w->cq, c);
	pos.y -= 2;
	HASH_FIND(hh, w->chunks, pos.raw, sizeof(ivec2s), c);
	if (c)
		chunk_queue_push(&w->cq, c);
	return generated_chunk;
}

void chunk_save(Chunk* c)
{

}

void chunk_free(Chunk* c)
{
	if (c->need_to_save)
		chunk_save(c);
	glDeleteBuffers(1, &c->vbo);
	free(c);
}

inline void chunk_set(Chunk* c, u8 x, u8 y, u8 z, u16 s)
{
	c->blk[y*CHUNK_SIZE_X*CHUNK_SIZE_Z + x*CHUNK_SIZE_Z + z] = s;
	c->changed = true;
	c->need_to_save = true;
}

inline void chunk_set_raw(Chunk* c, u32 blk, u16 s)
{
	c->blk[blk] = s;
	c->changed = true;
	c->need_to_save = true;
}

#define TRANSPARENT_CHECK(t) (BLOCKS[t].transparent && t != blk) 

void chunk_update(World* w, Chunk* c)
{
	c->changed = false;
	u8vec4s vertex[CHUNK_SIZE * 6 * 6];
	u32 i = 0;
	// Cache neighbor pointers
	Chunk* neighbors [4];
	ivec2 neighbor_pos = {c->id.x, c->id.y + 1};
	HASH_FIND(hh, w->chunks, neighbor_pos, sizeof(neighbor_pos), neighbors[POS_Z]);
	neighbor_pos[1] -= 2;
	HASH_FIND(hh, w->chunks, neighbor_pos, sizeof(neighbor_pos), neighbors[NEG_Z]);
	neighbor_pos[0]++;
	neighbor_pos[1]++;
	HASH_FIND(hh, w->chunks, neighbor_pos, sizeof(neighbor_pos), neighbors[POS_X]);
	neighbor_pos[0] -= 2;
	HASH_FIND(hh, w->chunks, neighbor_pos, sizeof(neighbor_pos), neighbors[NEG_X]);
	for (u8 y = 0; y < CHUNK_SIZE_Y; y++) {
		for (u8 x = 0; x < CHUNK_SIZE_X; x++) {
			for (u8 z = 0; z < CHUNK_SIZE_Z; z++) {
				u8 blk = chunk_get_block(c, x, y, z);
				if (!blk || blk >= MAX_BLOCK)
					continue;
				u8 type;
				u8 neighbor;
				// Will refactor later
				// -x
				if (x)
					neighbor = chunk_get_block(c, (x - 1), y, z);
				else
					if (neighbors[NEG_X])
						neighbor = chunk_get_block(neighbors[NEG_X], (CHUNK_SIZE_X - 1), y, z); 
					else
						neighbor = AIR;

				if (TRANSPARENT_CHECK(neighbor)) {
					type = BLOCKS[blk].get_texture_location(NEG_X);
					vertex[i++] = (u8vec4s){x,     y,     z,     type};        
        			vertex[i++] = (u8vec4s){x,     y,     z + 1, type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x,     y,     z + 1, type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z + 1, type};
        		}

				// +x
				if (x == (CHUNK_SIZE_X - 1)) {
					if (neighbors[POS_X])
						neighbor = chunk_get_block(neighbors[POS_X], 0, y, z);
					else
						neighbor = AIR;
				}
				else
					neighbor = chunk_get_block(c, (x + 1), y, z);

				if (TRANSPARENT_CHECK(neighbor)) {
					type = BLOCKS[blk].get_texture_location(POS_X);
					vertex[i++] = (u8vec4s){x + 1, y,     z,     type};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z + 1, type};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z + 1, type};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z + 1, type};
        		}

				// -y
				if (!y || TRANSPARENT_CHECK(chunk_get_block(c, x, (y - 1), z))) {
        			type = BLOCKS[blk].get_texture_location(NEG_Y);
					vertex[i++] = (u8vec4s){x,     y,     z,     type + 128};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z,     type + 128};        
        			vertex[i++] = (u8vec4s){x,     y,     z + 1, type + 128};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z,     type + 128};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z + 1, type + 128};        
        			vertex[i++] = (u8vec4s){x,     y,     z + 1, type + 128};
        		}

				// +y
				if (y == (CHUNK_SIZE_Y - 1) || TRANSPARENT_CHECK(chunk_get_block(c, x, (y + 1), z))) {
        			type = BLOCKS[blk].get_texture_location(POS_Y);
					vertex[i++] = (u8vec4s){x,     y + 1, z,     type + 128};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z + 1, type + 128};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z,     type + 128};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z,     type + 128};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z + 1, type + 128};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z + 1, type + 128};
        		}
				// -z
				if (z)
					neighbor = chunk_get_block(c, x, y, (z - 1));
				else
					if (neighbors[NEG_Z])
						neighbor = chunk_get_block(neighbors[NEG_Z], x, y, (CHUNK_SIZE_Z - 1)); 
					else
						neighbor = AIR;

				if (TRANSPARENT_CHECK(neighbor)) {
        			type = BLOCKS[blk].get_texture_location(NEG_Z);
					vertex[i++] = (u8vec4s){x,     y,     z,     type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z,     type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z,     type};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z,     type};
        		}
				// +z
				if (z == (CHUNK_SIZE_Z - 1)) {
					if (neighbors[POS_Z])
						neighbor = chunk_get_block(neighbors[POS_Z], x, y, 0); 
					else
						neighbor = AIR;
				}
				else
					neighbor = chunk_get_block(c, x, y, (z + 1));

				if (TRANSPARENT_CHECK(neighbor)) {
        			type = BLOCKS[blk].get_texture_location(POS_Z);
					vertex[i++] = (u8vec4s){x,     y,     z + 1, type};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z + 1, type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z + 1, type};        
        			vertex[i++] = (u8vec4s){x,     y + 1, z + 1, type};        
        			vertex[i++] = (u8vec4s){x + 1, y,     z + 1, type};
        			vertex[i++] = (u8vec4s){x + 1, y + 1, z + 1, type};        
        		}
			}
		}
	}
	c->elements = i;
  	glBindBuffer(GL_ARRAY_BUFFER, c->vbo);
  	glBufferData(GL_ARRAY_BUFFER, c->elements * sizeof *vertex, vertex, GL_STATIC_DRAW);
}

void chunk_render(World* w, Chunk* c)
{
	/*glEnable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tile_atlas.id);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);*/
	if (c->changed)
    	chunk_queue_push(&w->cq, c);

  	if (!c->elements)
    	return;

  	glBindBuffer(GL_ARRAY_BUFFER, c->vbo);
  	glVertexAttribPointer(attribute_coord, 4, GL_BYTE, GL_FALSE, 0, 0);
  	glDrawArrays(GL_TRIANGLES, 0, c->elements);
}

void world_seed_generate(World_Seed *s)
{
	s->height_map = rand();	
	s->heat_map = rand();	
	s->humidity_map = rand();	
	s->structure_map = rand();	
}

#define RENDER_SIZE (2*w->rend_dist + 1)*(2*w->rend_dist + 1)

void world_init(World* w, u8 rend_dist)
{
	world_seed_generate(&w->seed);
	w->rend_dist = rend_dist;
	w->sorted_chunks = malloc(sizeof(*w->sorted_chunks)*RENDER_SIZE);
	memset(w->sorted_chunks, 0, sizeof(*w->sorted_chunks)*RENDER_SIZE);
	program = shader_create_program("src/shaders/world.v.glsl", "src/shaders/world.f.glsl");

	attribute_coord = glGetAttribLocation(program, "coord");
	uniform_mvp = glGetUniformLocation(program, "mvp");
	PANIC(attribute_coord == -1 || uniform_mvp == -1, "Could not get location of attribute coord and or uniform mvp!\r\n");

	stbi_set_flip_vertically_on_load(true);
	tile_atlas.data = stbi_load("images/tile_atlas.png", &tile_atlas.width, &tile_atlas.height, &tile_atlas.id, 4);
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tile_atlas.id);
	glBindTexture(GL_TEXTURE_2D, tile_atlas.id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tile_atlas.width, tile_atlas.height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, tile_atlas.data);
	stbi_image_free(tile_atlas.data);

	glUseProgram(program);
	glClearColor(0.6, 0.8, 1.0, 0.0);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_POLYGON_OFFSET_FILL);
	//glPolygonOffset(1, 1);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // Use GL_NEAREST_MIPMAP_LINEAR if you want to use mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glEnableVertexAttribArray(attribute_coord);

	glm_vec3_copy(&(vec3s){0, CHUNK_SIZE_Y + 1, 0}, w->position);
	w->old_position[0] = w->position[0] + 16;
	glm_vec3_copy(&(vec3s){0, 0, 0}, w->angle);

	// Hash table
	w->chunks = NULL;
	chunk_queue_init(&w->cq, 1024);
}

void chunk_attempt_to_free(World* w, Chunk* c)
{
	if (c->is_queued) {
		c->scrapped = true;
		return;
	}
	HASH_DEL(w->chunks, c);
	chunk_free(c);
	// printf("Free'd at %d, %d\n", pos.x, pos.y);
}

void world_render(World* w)
{
	mat4 view;
	vec3 center;
	glm_vec3_add(w->position, w->lookat, center);
	glm_lookat(w->position, center, w->up, view);
	mat4 projection;
	glm_perspective(45.0f, (float)state.window_width/state.window_height, 0.001f, 1000.0f, projection);

	chunk_queue_poll(w);

	mat4 pv, mvp;
	glm_mat4_mul(projection, view, pv);
	if (floor(w->old_position[0] / 16) != floor(w->position[0] / 16) || floor(w->old_position[1] / 16) != floor(w->position[2] / 16)) {
		// Loop through all the chunks rendered last frame and free them if they are no longer relevant
		for (u32 i = 0; i < RENDER_SIZE; i++) {
			if (w->sorted_chunks[i].c) {
				// Is it not inside the render radius anymore
				if ((w->sorted_chunks[i].c->id.x < floor(w->position[0] / 16) - w->rend_dist || w->sorted_chunks[i].c->id.x > floor(w->position[0] / 16) + w->rend_dist) ||
						(w->sorted_chunks[i].c->id.y < floor(w->position[2] / 16) - w->rend_dist || w->sorted_chunks[i].c->id.y > floor(w->position[2] / 16) + w->rend_dist))
					chunk_attempt_to_free(w, w->sorted_chunks[i].c);
			}
		}
	}

	// Add chunks that need to be rendered to array
	u32 k = 0;
	ivec2 pos;
	memset(w->sorted_chunks, 0, sizeof(*w->sorted_chunks)*RENDER_SIZE);
	for (pos[1] = ((i32)floor(w->position[2] / 16.0f) - w->rend_dist); pos[1] <= ((i32)floor(w->position[2] / 16.0f) + w->rend_dist); pos[1]++) {
		for (pos[0] = ((i32)floor(w->position[0] / 16.0f) - w->rend_dist); pos[0] <= ((i32)floor(w->position[0] / 16.0f) + w->rend_dist); pos[0]++) {
			mat4 model = GLM_MAT4_IDENTITY_INIT;
			glm_translate(model, &(vec3s){pos[0]*16, 0.0f, pos[1]*16});
			glm_mat4_mul(pv, model, mvp);

			// Is this chunk on the screen?
			vec4s chunk_center;
			glm_mat4_mulv(mvp, &(vec4){CHUNK_SIZE_X / 2, CHUNK_SIZE_Y / 2, CHUNK_SIZE_Z / 2, 1}, chunk_center.raw);

			chunk_center.x /= chunk_center.w;
			chunk_center.y /= chunk_center.w;

			// If it is behind the camera, don't bother drawing it
			if(chunk_center.z < -CHUNK_SIZE_Y / 2)
				continue;

			// If it is outside the screen, don't bother drawing it
			if(fabsf(chunk_center.x) > 1 + fabsf(CHUNK_SIZE_Y * 2 / chunk_center.w) || fabsf(chunk_center.y) > 1 + fabsf(CHUNK_SIZE_Y * 2 / chunk_center.w))
				continue;

			HASH_FIND(hh, w->chunks, pos, sizeof(ivec2s), w->sorted_chunks[k].c);
			if (!w->sorted_chunks[k].c)
				w->sorted_chunks[k].c = chunk_generate(w, pos);
			w->sorted_chunks[k].dist = -chunk_center.z;
			++k;
		}

		// Sort Chunks
		quicksort(w->sorted_chunks, 0, RENDER_SIZE - 1);
	}

	for (u32 i = 0; i < RENDER_SIZE; i++) {
		if (w->sorted_chunks[i].c) {
			mat4 model = GLM_MAT4_IDENTITY_INIT;
			glm_translate(model, &(vec3s){w->sorted_chunks[i].c->id.x*16, 0.0f, w->sorted_chunks[i].c->id.y*16});
			glm_mat4_mul(pv, model, mvp);

			glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, mvp);
			//printf("%d, %d\n", pos.x, pos.y);
			chunk_render(w, w->sorted_chunks[i].c);
		}
	} 
	w->old_position[0] = w->position[0];
	w->old_position[1] = w->position[2];
}
