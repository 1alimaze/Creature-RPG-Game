#include "errors.h"
#include "world.h"

void chunk_queue_init(Chunk_Queue *cq, u64 event_size)
{
	cq->chunks = malloc(sizeof(Chunk*)*event_size);
	PANIC(!cq->chunks, "Failed to allocate chunk queue!\n");
	cq->size = event_size;
	cq->head = 0;
	cq->tail = 0;
	//chunk_queue_flush(cq);
}

void chunk_queue_push(Chunk_Queue* cq, Chunk* c)
{	
	if (c->is_queued)
		return;
	c->is_queued = true;
	cq->chunks[cq->tail++] = c;
	cq->tail %= cq->size;
}

void chunk_queue_poll(World* w)
{
	Chunk_Queue* cq = &w->cq;
	if (cq->head == cq->tail)
		return;
	if (cq->chunks[cq->head]->scrapped) {
		HASH_DEL(w->chunks, cq->chunks[cq->head]);
		chunk_free(cq->chunks[cq->head]);
	}
	cq->chunks[cq->head]->is_queued = false;
	chunk_update(w, cq->chunks[cq->head++]);
	cq->head %= cq->size;
}

void chunk_queue_flush(Chunk_Queue* cq)
{
	cq->head = 0;
	cq->tail = 0;
}
