#include "block.h"

static u8 get_texture_location(enum Direction d)
{
    return TILE_WATER;
}

void water_init()
{
    Block* water = BLOCKS + WATER;
    water->id = WATER;
    water->transparent = true;
    water->get_texture_location = get_texture_location;
}
