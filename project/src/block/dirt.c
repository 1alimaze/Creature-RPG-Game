#include "block.h"

static u8 get_texture_location(enum Direction d)
{
    return TILE_DIRT;
}

void dirt_init()
{
    Block* dirt = BLOCKS + DIRT;
    dirt->id = DIRT;
    dirt->transparent = false;
    dirt->get_texture_location = get_texture_location;
}
