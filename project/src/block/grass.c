#include "block.h"

static u8 get_texture_location(enum Direction d)
{
    if (d < UP)
        return TILE_GRASS_SIDE;
    if (d == UP)
        return TILE_GRASS_TOP;
    return TILE_DIRT;
}

void grass_init()
{
    Block* grass = BLOCKS + GRASS;
    grass->id = GRASS;
    grass->transparent = false;
    grass->get_texture_location = get_texture_location;
}
