#include "block.h"

static u8 get_texture_location(enum Direction d)
{
    return TILE_SAND;
}

void sand_init()
{
    Block* sand = BLOCKS + SAND;
    sand->id = SAND;
    sand->transparent = false;
    sand->get_texture_location = get_texture_location;
}
