#version 120
varying vec4 texcoord;
uniform sampler2D texture;

const vec4 fogcolor = vec4(0.6, 0.8, 1.0, 1.0);
const float fogdensity = .00003;

void main(void) {
  vec4 color;
  if (texcoord.w < 0) {
    color = texture2D(texture, vec2((fract(texcoord.x) + texcoord.w) / 16.0, texcoord.z));
    color.rgb *= 1.25;
  }
  else
    color = texture2D(texture, vec2((fract(texcoord.x + texcoord.z) + texcoord.w) / 16.0, texcoord.y));
  
  float z = gl_FragCoord.z / gl_FragCoord.w;
  float fog = clamp(exp(-fogdensity * z * z), 0.2, 1);

  gl_FragColor = mix(fogcolor, color, fog);
}
