#ifndef CHUNK_QUEUE_H
#define CHUNK_QUEUE_H

typedef struct {
	Chunk** chunks;
	u64 head, tail;
	u64 size;
} Chunk_Queue;

extern void chunk_queue_init(Chunk_Queue* cq, u64 event_size);
extern void chunk_queue_push(Chunk_Queue* cq, Chunk* c);
extern void chunk_queue_poll(World* w);
extern void chunk_queue_flush(Chunk_Queue* cq);

#endif /* CHUNK_QUEUE_H */
