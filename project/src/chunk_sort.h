#ifndef CHUNK_SORT
#define CHUNK_SORT

typedef struct {
	Chunk* c;
	float dist;
} Sorted_Chunk;

extern void quicksort(Sorted_Chunk* arr, int low, int high);

#endif // CHUNK_SORT
