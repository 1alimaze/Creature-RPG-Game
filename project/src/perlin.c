#include <stdio.h>
#include <math.h>
#include "data_types.h"

/*
 * Shamelessly stolen from czinn.
 * I didn't use my perlin version because I didn't know how to octaves and persistence
 * Credit: https://github.com/czinn/perlin/tree/master
*/

f64 rawnoise(i32 n)
{
    n = (n << 13) ^ n;
    return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

f64 noise1d(i32 x, i32 octave, i32 seed)
{
    return rawnoise(x * 1619 + octave * 3463 + seed * 13397);
}

f64 noise2d(i32 x, i32 y, i32 octave, i32 seed)
{
    return rawnoise(x * 1619 + y * 31337 + octave * 3463 + seed * 13397);
}

f64 noise3d(i32 x, i32 y, i32 z, i32 octave, i32 seed)
{
    return rawnoise(x * 1919 + y * 31337 + z * 7669 + octave * 3463 + seed * 13397);
}

f64 interpolate(f64 a, f64 b, f64 t)
{
    // Smooth interpolate
    f64 f = (1 - cos(t * 3.141593)) * 0.5;

    return a * (1 - f) + b * f;

    // Fast interpolate
    // return (b - a) * t + a;
}

f64 smooth1d(f64 x, i32 octave, i32 seed)
{
    i32 int_x = floor(x);
    f64 fracx = x - int_x;

    f64 v1 = noise1d(int_x, octave, seed);
    f64 v2 = noise1d(int_x + 1, octave, seed);

    return interpolate(v1, v2, fracx);
}

f64 smooth2d(f64 x, f64 y, i32 octave, i32 seed)
{
    // Casting to i32 was being weird
    i32 int_x = floor(x);
    f64 fracx = x - int_x;
    i32 int_y = floor(y);
    f64 fracy = y - int_y;
    
    f64 v1 = noise2d(int_x, int_y, octave, seed);
    f64 v2 = noise2d(int_x + 1, int_y, octave, seed);
    f64 v3 = noise2d(int_x, int_y + 1, octave, seed);
    f64 v4 = noise2d(int_x + 1, int_y + 1, octave, seed);
    
    f64 i1 = interpolate(v1, v2, fracx);
    f64 i2 = interpolate(v3, v4, fracx);
    
    return interpolate(i1, i2, fracy);
}

f64 smooth3d(f64 x, f64 y, f64 z, i32 octave, i32 seed)
{
    i32 int_x = floor(x);
    f64 fracx = x - int_x;
    i32 int_y = floor(y);
    f64 fracy = y - int_y;
    i32 int_z = floor(z);
    f64 fracz = z - int_z;


    f64 v1 = noise3d(int_x, int_y, int_z, octave, seed);
    f64 v2 = noise3d(int_x + 1, int_y, int_z, octave, seed);
    f64 v3 = noise3d(int_x, int_y + 1, int_z, octave, seed);
    f64 v4 = noise3d(int_x + 1, int_y + 1, int_z, octave, seed);
    f64 v5 = noise3d(int_x, int_y, int_z + 1, octave, seed);
    f64 v6 = noise3d(int_x + 1, int_y, int_z + 1, octave, seed);
    f64 v7 = noise3d(int_x, int_y + 1, int_z + 1, octave, seed);
    f64 v8 = noise3d(int_x + 1, int_y + 1, int_z + 1, octave, seed);

    f64 i1 = interpolate(v1, v2, fracx);
    f64 i2 = interpolate(v3, v4, fracx);
    f64 i3 = interpolate(v5, v6, fracx);
    f64 i4 = interpolate(v7, v8, fracx);
    
    f64 j1 = interpolate(i1, i2, fracy);
    f64 j2 = interpolate(i3, i4, fracy);

    return interpolate(j1, j2, fracz);
}

f64 pnoise1d(f64 x, f64 persistence, i32 octaves, i32 seed)
{
   f64 total = 0.0;
   f64 frequency = 1.0;
   f64 amplitude = 1.0;
   i32 i = 0;
   
   for(i = 0; i < octaves; i++) {
       total += smooth1d(x * frequency, i, seed) * amplitude;
       frequency /= 2;
       amplitude *= persistence;
   } 

   return total;
}

f64 pnoise2d(f64 x, f64 y, f64 persistence, i32 octaves, i32 seed)
{
   f64 total = 0.0;
   f64 frequency = 1.0;
   f64 amplitude = 1.0;
   i32 i = 0;
   
   for(i = 0; i < octaves; i++) {
       total += smooth2d(x * frequency, y * frequency, i, seed) * amplitude;
       frequency /= 2;
       amplitude *= persistence;
   } 

   return total;
}

f64 pnoise3d(f64 x, f64 y, f64 z, f64 persistence, i32 octaves, i32 seed)
{
   f64 total = 0.0;
   f64 frequency = 1.0;
   f64 amplitude = 1.0;
   i32 i = 0;
   
   for(i = 0; i < octaves; i++) {
       total += smooth3d(x * frequency, y * frequency, z * frequency, i, seed) * amplitude;
       frequency /= 2;
       amplitude *= persistence;
   } 

   return total;
}

// Helper function for normalization of perlin noise
f64 normalize1d(f64 input)
{
    return (input*input) / (1 + input*input);
}

f64 sigmoid(f64 input)
{
    return 1.0 / (1 + exp(-input));
}
